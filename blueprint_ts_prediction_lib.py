"""
blueprint_ts_prediction.py library of functions
"""

import pandas as pd
import numpy as np
import os
import math
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.arima_model import ARIMA
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
import matplotlib.pyplot as plt


def import_data(dirIn):
    """
    Imports and concatenates all data from the specified DATA directory
    Returns a single concatenated DataFrame
    """
    # Iterate through all subdirs in main data folder
    for subdir in os.listdir(dirIn):
        # Skip if subdir is a file
        if "." in subdir:
            continue
        for file in os.listdir(dirIn + subdir):
            # Skip if not a csv file
            if ".csv" not in file:
                continue
            df = pd.read_csv(dirIn + subdir + "/" + file)

            # Concat current data with previous dataframes
            if 'df_concat' not in locals():
                df_concat = df
            else:
                df_concat = pd.concat([df_concat, df])
    return df_concat


def clean_data(df):
    """
    Renames columns to shorter names
    Removes the PTID column which is redundant to the Name column
    Converts timestamp to datetime object and sets as index
    """

    # Define new names
    rename_dict = {
    "Time Stamp": "Timestamp",
    "Name": "Location",
    "LBMP ($/MWHr)": "LBMP",
    "Marginal Cost Losses ($/MWHr)": "MCL",
    "Marginal Cost Congestion ($/MWHr)": "MCC"
    }

    # Rename Columns
    df = df.rename(columns = rename_dict)
    # Drop the PTID column
    df = df.drop("PTID", axis=1)

    # Set Timestamp to sorted datetime index
    df["Timestamp"] = pd.to_datetime(df["Timestamp"])
    df = df.set_index("Timestamp").sort_index()

    return df

def create_plots(df, plots_dir):
    """
    Creates plots for data exploration on raw cleaned data set:

    Partial Autocorrelation of LBMP_N.Y.C.
    """
    # Plot Histogram of N.Y.C. LBMP
    plt.figure()
    plt.hist(df["LBMP_N.Y.C."], bins=25)
    plt.title("N.Y.C. LBMP Histogram")
    plt.xlabel("LBMP ($/MWh)")
    plt.savefig(plots_dir + "Histogram.png")
    plt.close()

    # Seasonal Decomposition of LBMP_N.Y.C.
    sd_nyc = seasonal_decompose(df["LBMP_N.Y.C."], model="additive", freq=24*7)
    plt.figure(figsize=[8,6])
    plt.subplot(3,1,1)
    plt.plot(sd_nyc.observed); plt.ylabel("Observed")
    plt.title("Seasonal Decomposition of N.Y.C. LBMP ($/MWh)")
    plt.subplot(3,1,2)
    plt.plot(sd_nyc.trend); plt.ylabel("Trend")
    plt.subplot(3,1,3)
    plt.plot(sd_nyc.seasonal[0:24*7]); plt.ylabel("Seasonal")
    plt.xticks(rotation=45)
    plt.savefig(plots_dir + "SeasonalDecomp.png")
    plt.close()

    # Partial Autocorrelation
    fig = plot_pacf(df["LBMP_N.Y.C."], lags=180)
    fig.set_size_inches(8,6)
    plt.title("N.Y.C. LBMP Partial Autocorrelation Plot")
    plt.xlabel("Time Lag (hrs)")
    plt.ylabel("PACF")
    plt.grid()
    plt.savefig(plots_dir + "PacfPlot.png")
    plt.close()

    # Plot Heatmap of Correlation Matrix
    plt.figure(figsize=[8,6])
    heatmap(df.corr().apply(abs), list(df), list(df))
    plt.title("Correlation Matrix (Abs Value)")
    plt.tight_layout()
    plt.savefig(plots_dir + "CorrHmap.png")
    plt.close()

    return

def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Arguments:
        data       : A 2D numpy array of shape (N,M)
        row_labels : A list or array of length N with the labels
                     for the rows
        col_labels : A list or array of length M with the labels
                     for the columns
    Optional arguments:
        ax         : A matplotlib.axes.Axes instance to which the heatmap
                     is plotted. If not provided, use current axes or
                     create a new one.
        cbar_kw    : A dictionary with arguments to
                     :meth:`matplotlib.Figure.colorbar`.
        cbarlabel  : The label for the colorbar
    All other arguments are directly passed on to the imshow call.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=90)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=2)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar

def clean_arima(df, dt_predict, duration_hrs):
    """
    Prepare data for ARIMA model
    Split data into train and test
    Returns train, test, and the test_idx (datetime index of test data)
    """
    print("Cleaning data for ARIMA...")
    df = df[df.Location == "N.Y.C."]

    # Set test start back a day and to 5am (according to day-ahead markets on NYISO)
    test_start = (pd.date_range(dt_predict, periods=1) - pd.Timedelta(days=1))[0]
    test_start = test_start.replace(hour = 5)
    test_end = (pd.date_range(dt_predict, periods=1) + pd.Timedelta(hours=duration_hrs))[0]

    # Split Data
    train = df[df.index < test_start]
    test = df[(df.index >= test_start) & (df.index < test_end)]

    test_idx = test.index

    train = train["LBMP"].values
    test = test["LBMP"].values


    return train, test, test_idx

def run_arima(train, test, test_idx):
    """
    Fit and predict the ARIMA model for the whole test period
    Return the predictions array
    """
    print("Fitting ARIMA model...")
    history = [x for x in train]
    predictions = list()
    for t in range(len(test)):
        model = ARIMA(history, order=(5,1,0))
        model_fit = model.fit(disp=0)
        output = model_fit.forecast()
        yhat = output[0]
        predictions.append(yhat)
        obs = test[t]
        history.append(obs)
        # print("predicted=%f, expected=%f" % (yhat, obs))
    error = math.sqrt(mean_squared_error(test, predictions))
    print("Test RMSE: {}".format(error))
    return predictions

def get_arima_results(predictions, test, test_idx, dt_predict):
    """
    Displays the results from the ARIMA model for the defined prediction period
    Displays as plot if more than 1 time step predicted. Prints result if only 1 timestep
    Returns results dataframe
    """
    print("Getting ARIMA results...")
    results = pd.DataFrame(predictions, index=test_idx, columns=["LBMP_Predicted"])
    results["LBMP_TestData"] = test
    #test_df = pd.DataFrame(test, index = test_idx, columns=["LBMP_TestData"])

    # Filter to only the desired forecast period
    results = results[results.index >= dt_predict]
    #test_df = test_df[test_df.index >= dt_predict]

    if results.shape[0] == 1:
        print(results)
    else:
        # Plot Results
        plt.plot(results["LBMP_TestData"], label="Test")
        plt.plot(results["LBMP_Predicted"], label="Predicted")
        plt.legend()
        plt.xticks(rotation=45)
        plt.ylabel("LBMP ($/MWh)")
        plt.title("ARIMA Day Ahead Forecast of N.Y.C. LBMP")
    return results


def add_time_features(df):
    """
    Processes data for the basic LSTM model
    Filters data for only N.Y.C. rows
    Creates new features in df for day of week, hour of day, and month
    """

    df["Month"] = df.index.month
    df["DOW"] = df.index.dayofweek
    df["Hour"] = df.index.hour
    return df

def reshape_data(df):
    """
    Pivots the table so that each row is a single timestamp with
    separate columns for data corresponding to the different locations

    Rejoins LBMP data with cost from N.Y.C.
    """

    df = df.groupby(["Timestamp", "Location"]).first().reset_index()

    # Save Cost Data of NYC
    cost_nyc = df[df.Location=="N.Y.C."][["Timestamp", "MCL", "MCC"]]
    cost_nyc = cost_nyc.rename(columns={"MCL":"MCL_NYC", "MCC":"MCC_NYC"})
    cost_nyc = cost_nyc.set_index("Timestamp")

    # Pivot LBMP Data
    df = df.pivot(index="Timestamp", columns="Location", values="LBMP")

    # Rename Columns
    for col in list(df):
        df = df.rename(columns={col: "LBMP_"+col})

    # Rejoin LBMP and Cost Dataframes
    df = df.join(cost_nyc)

    return df

def lstm_preprocess(df, Xy_vars, feature_lag):
    """
    Normalizes data to MinMax 0-1
    Reframes data for supervised learning to 1 timestep as input to predict LBMP
    """
    var_list = list(df)
    values = df.values

    # Normlize features
    scaler = MinMaxScaler(feature_range=(0,1))
    scaled = scaler.fit_transform(values)

    # frame as supervised learning
    reframed = series_to_supervised(scaled, lags=[feature_lag], n_out=1)

    return scaler, reframed, var_list


def series_to_supervised(data, lags=[1], n_out=1, dropnan=True):
    # This fucntion converts the timeseries into a supervised learning array
    # with a specified range of time lags from max_lag to min_lag
	n_vars = 1 if type(data) is list else data.shape[1]
	df = pd.DataFrame(data)
	cols, names = list(), list()
	# input sequence (t-n, ... t-1)
	for i in lags:
		cols.append(df.shift(i))
		names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
	# forecast sequence (t, t+1, ... t+n)
	for i in range(0, n_out):
		cols.append(df.shift(-i))
		if i == 0:
			names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
		else:
			names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
	# put it all together
	agg = pd.concat(cols, axis=1)
	agg.columns = names
	# drop rows with NaN values
	if dropnan:
		agg.dropna(inplace=True)
	return agg




def create_X_var_list(feature_lag):
    """
    Produces the list of variable inputs to feed to the LSTM from the supervised-
    structured dataframe data_supervised.
    Type 1: Selects all LBMP and Cost values from (t-48) lag [Vars 1-17]
    Type 2: Selects Month, Day of Week, and Hour for (t) - no lag. [Vars 18-20]
        these are known values ahead of time
    Returns a list a variable names that will make up the input matrix
    """
    X_var_list =  []

    # Add type 1 variables
    X_var_list = []
    for n in range(1,18):
        X_var_list.append(var_str(n, feature_lag))

    # Add type 2 variables
    for n in range(18,21):
        X_var_list.append(var_str(n, 0))

    return X_var_list

def create_y_var_name(var_list):
    """
    Creates the output var name in the vn(t-L) format based on the location of
    'LBMP_N.Y.C.' in the var_list
    """
    n = var_list.index("LBMP_N.Y.C.") + 1
    y_var_name = "var{}(t)".format(n)
    return y_var_name

def append_var_lists(X_var_list, y_var_name):
    """
    Appends the y_var_name to the list of X vars X_var_list
    Returns the full list of variable names of the Xy matrix
    """
    Xy_vars = np.append(X_var_list, y_var_name).tolist()
    return Xy_vars


def var_str(n, lag):
    """
    Returns the variable name string given var number and lag value
    """
    if lag == 0:
        var = "var{}(t)".format(n)
    else:
        var = "var{}(t-{})".format(n, lag)
    return var


def split_train_test(df, dt_split_string, original_series):
    """
    Splits the data into train and test based on the date specified as a string
    from the input argument dt_split_string
    Returns train, test, and the test_idx (datetime index) arrays
    """
    # Get the number of hours in dataset AFTER the train/test cutoff date
    test_originalData = original_series[original_series.index >= dt_split_string]
    test_hrs = test_originalData.shape[0]
    test_idx = test_originalData.index

    # Split Data
    train = df.iloc[:-test_hrs,:]
    test = df.iloc[-test_hrs:,:]

    return train, test, test_idx

def split_Xy(df, X_var_list, y_var_name):
    """
    Splits data into features: df_X and ouputs: df_y
    Returns as arrays
    """

    df_X = df[X_var_list]
    df_y = df[y_var_name]

    # Convert dataframe to array
    df_X = df_X.values
    df_y = df_y.values

    # Reshape input to be 3D [samples, timesteps, features]
    df_X = df_X.reshape((df_X.shape[0], 1, df_X.shape[1]))

    return df_X, df_y


def fit_LSTM(train_X, train_y, test_X, test_y):
    """
    Design Network
    Fit data to model
    Returns fitted model and fit history
    """

    # Design Network
    model = Sequential()
    model.add(LSTM(50, input_shape=(train_X.shape[1], train_X.shape[2])))
    model.add(Dense(1))
    model.compile(loss='mae', optimizer='adam')

    # Fit Model
    history = model.fit(train_X, train_y, epochs=50, batch_size=72, validation_data=(test_X, test_y), verbose=2, shuffle=False)

    return model, history

def predict_lstm(model, test_X, test_y, test_idx, forecast_duration, lstm_scaler):
    """
    Predicts test data test_X on model
    Displays RMSE
    Returns the prediction and observed test values in a dataframe
    """
    # make a prediction
    yhat = model.predict(test_X)
    test_X_2d = test_X.reshape((test_X.shape[0], test_X.shape[2]))

    # invert scaling for forecast
    concat_yhat = np.concatenate((test_X_2d[:,0:10], yhat, test_X_2d[:,11:]), axis=1)
    concat_yhat_inverted = lstm_scaler.inverse_transform(concat_yhat)
    inv_yhat = concat_yhat_inverted[:,10]
    # invert scaling for actual
    test_y_2d = test_y.reshape((len(test_y), 1))
    #concat_y = np.concatenate((test_y_2d, test_X_2d[:,1:]), axis=1)
    concat_y = np.concatenate((test_X_2d[:,0:10], test_y_2d, test_X_2d[:,11:]), axis=1)
    inv_concat_y = lstm_scaler.inverse_transform(concat_y)
    inv_y = inv_concat_y[:,10]
    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(inv_y, inv_yhat))
    print('Test RMSE: %.3f' % rmse)


    # Create Results DataFrame
    results = pd.DataFrame(inv_yhat[0:forecast_duration], index=test_idx[0:forecast_duration], columns=["LBMP_Predicted"])
    results["LBMP_TestData"] = inv_y[0:forecast_duration]

    return results

def display_lstm_prediction(results, plots_dir):
    """
    Prints results if prediction duration is single hour
    Plots results if prediction duration is multiple timesteps
    Saves fig to plots_dir
    """
    if results.shape[0] == 1:
        print(results)
    else:
        plt.figure()
        plt.plot(results["LBMP_TestData"], label="TestData")
        plt.plot(results["LBMP_Predicted"], label="Predicted")
        plt.legend()
        plt.xticks(rotation=45)
        plt.ylabel("LBMP ($/MWh)")
        plt.title("LSTM N.Y.C. Day Ahead Prediction")
        plt.tight_layout()
        plt.savefig(plots_dir + "PredictionVsActual.png")
        plt.close()
    return

def plot_history(history, plots_dir):
    """
    Plots history of fitting
    Saves fig to plots_dir
    """
    plt.plot(history.history["loss"], label="train")
    plt.plot(history.history["val_loss"], label="test")
    plt.legend()
    plt.title("LSTM Fit")
    plt.ylabel("Loss")
    plt.xlabel("Epoch")
    plt.tight_layout()
    plt.savefig(plots_dir + "LSTM_history.png")
    plt.close()
    return
