"""
Dataset includes folders for each month.
Each folder has files for each day
Each file contains 1 hour increments of data for each location
Each location has data for ID, marginal price, and marginal costs of losses and congestion

Creates data visualizations from initial exploration
Commented out ARIMA model
Runs an LSTM model

"""

import blueprint_ts_prediction_lib as lib

#################################
# Define Run Parameters

# Define Data Folder location
dirIn = "./2017_NYISO_LBMPs/"
dirPlots = "./PLOTS/"

# Define date of split between train and test data
datetime_predict = "2017-08-01" # Use Format: 'YYYY-mm-dd' OR 'YYYY-mm-dd HH:MM:SS'
forecast_duration = 24 # hrs

# Define Prediction Lag for LSTM
lstm_feature_lag = 48 # hrs


#################################
# Import and Preprocess Data

data = lib.import_data(dirIn)

data = lib.clean_data(data)

data_reshaped = lib.reshape_data(data)


################################
# Data Exploration

lib.create_plots(data_reshaped, dirPlots)


################################
# Model Attempt 1 - ARIMA
"""
First created an ARIMA model that only predicts based on the straight NYC time series
This model is not sufficient because it uses previous hour data in prediction
which does not satisfy day ahead requirements
"""


"""
[arima_train, arima_test, test_idx] = lib.clean_arima(data, datetime_predict, forecast_duration)

predictions = lib.run_arima(arima_train, arima_test, test_idx)

arima_results = lib.get_arima_results(predictions, arima_test, test_idx, datetime_predict)
"""


################################
# Model Attempt 2 - LSTM
"""
Then Create an LSTM model with 48 hour ahead predictions
"""

# Create data for first LSTM model
data_reshaped = lib.add_time_features(data_reshaped)

# Create Supervised Learning Variable Names
X_var_list = lib.create_X_var_list(lstm_feature_lag)
y_var_name = lib.create_y_var_name(list(data_reshaped))
Xy_vars = lib.append_var_lists(X_var_list, y_var_name)

# Normalize Data and reframe for supervised learning
[lstm_scaler, data_supervised, var_list] = lib.lstm_preprocess(data_reshaped, Xy_vars, lstm_feature_lag)

# Split into train and test sets
[train, test, test_idx] = lib.split_train_test(data_supervised, datetime_predict, data_reshaped)

# Split into inputs and outputs
[train_X, train_y] = lib.split_Xy(train, X_var_list, y_var_name)
[test_X, test_y] = lib.split_Xy(test, X_var_list, y_var_name)

# Run LSTM and plot fit results
[model, history] = lib.fit_LSTM(train_X, train_y, test_X, test_y)
lib.plot_history(history, dirPlots)

# Predict on test data
lstm_results = lib.predict_lstm(model, test_X, test_y, test_idx, forecast_duration, lstm_scaler)
lib.display_lstm_prediction(lstm_results, dirPlots)
