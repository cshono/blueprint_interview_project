# blueprint_interview_project

How to Run: 

1. Download and unzip the project folder onto your machine. 
2. Unzip the 2017_NYISO_LBMPs.zip in the main project folder under the same name
   "2017_NYISO_LBMPs" as specified in the run parameters of blueprint_ts_prediction.py. 
   Each month should have its own subfolder within "2017_NYISO_LBMPs" with all of the CSVs inside. 
3. Ensure the proper Python environment with the following packages is installed: 
*  **pandas**
*  **numpy**
*  **os**
*  **math**
*  **statsmodels**
*  **sklearn**
*  **keras** (with either theanos or tensorflow backend) 
*  **matplotlib**
*  Note: See below for the exact package versions used 

4. Modify run parameters as desired at the top of blueprint_ts_prediction.py
*  **dirIn**(str): unzipped folder with all data provided data files 
*  **dirPlots** (str): folder location where plots will be saved 
*  **datetime_predict** (str): specify the start date [and optionally the time] for the model to predict with either the format: "**YYYY-mm-dd**" OR "**YYYY-mm-dd HH:MM:SS**"
*  **forecast_duration** (int): specify the number of hours for the model to predict. Note: Specify value of 1 for a single timestep prediction. 
*  **lstm_feature_lag** (int): specify the hours of lag to use for model features 
5. Run blueprint_ts_prediction.py 
6. Check the plots directory (dirPlots) for the model results. Save these imagies to a differnt 
    location if you wish to save them. Otherwise the code will overwrite these files 
    the next time it runs. 

Description of the Model: 

The requirement of this model is to predict the N.Y.C. location based marginal price (LMBP)
of electricity for any date and time. Hourly LMBP (and marginal cost) data was provided
for all of 2017 for 15 areas under the NYISO. Initial data exploration showed strong 
correlation between the prices of different areas. Additionally, LMBP exhibited
seasonal behavior on the daily and weekly timescales with prices typically peaking
in the afternoon/evening. A plot of partial autorrelation reveals significant 
time lag correlations of the N.Y.C. LBMP for time lags especially under 24 hours and also 
at multiples of 24 hours. Supporting plots of the above analysis can be found in the 
PLOTS folder of this project. 

Given the purpose of the day ahead model, time lags under 24 hours were not used 
for prediction of electricity prices. According to the NYISO day ahead sceduling manual
(https://www.nyiso.com/documents/20142/2923301/dayahd_schd_mnl.pdf/0024bc71-4dd9-fa80-a816-f9f3e26ea53a), 
day ahead bids are to be placed by 5 AM the day before dispatch. Therefore, most LBMP 
predictions must forecast about 1.5 days ahead. Given the strong correlation to 
lags of 24x hours, the dataset was featurized to a 48 hour time lag. 
Therefore, the 48 hr ahead price for all 15 regions along with the N.Y.C.-specific marginal costs
were used to predict the N.Y.C LBMP. Additionally, features for month, day of week, and hour of day were included
in the model. 

A Long short-term memory (LSTM) model was implemented. After brief research, Autoregressive
Integrative Moving Average (ARIMA) and LSTM were the most common time series models 
in use. LSTM was selected over ARIMA because of its flexibility in handling 
more input features while the ARIMA model typically only considers a single timeseries.

Because the project asks to predict each hour on August 1 of 2017, the model was split
to train on all data prior to this date. If a different prediction date is specified
in the run parameters, upon execution, the model will partition the data according to the new specified prediction date. 
The printed RMSE from the code is based on fit to all test data after the 
start of the specified prediction date. The timeframe over which predictions are 
plotted is specified by the forecast_duration parameter. The model will print a single
prediction and the corresponding actual value if the forecast_duration is set to 1. 

I hard-coded the LSTM to fit with epoch=50 and batch_size=72. I am not too familiar 
with neural nets, so I did not do much with these parameters. The predictions of 
the model follow the general daily peak pattern, but it does not reliably anticipate 
the upcoming days changes compared to the previous day. For example, if the duration
is set to 4 weeks (forecast_duration=28*24), it is clear that the first day with a sharp increase or decrease in
price is missed by the model, and by the following day, the model adjusts its prediction 
accordingly. This likely has to do with the longer prediction time horizon of 48 hours, which 
is much less strongly correlated to LBMP than the 24 hour time lag. This can be seen 
in the PredictedVsActual_4weeks.png plot. The prediction's high and low extreme days lag about 2 
days behind the observed high and low extreme days. 

If I had more time, I would have liked to try including the LBMP_NYC(t-72) in addition 
to the (t-48) as a feature which could have provided more information to the model about 
further back than just 2 days worth of reference point data. Another key data input that would improve the model 
would be weather forecast data. Weather forecasts are crucial to predicting the energy demand 
driven by heating/cooling, and also provide insight into wind and solar generation 
that can effect the price of electricity. The model would likely also benefit from 
training on data from both 2016 and 2017. When training on only a partial year's worth 
of data, the model must extrapolate to predict prices during seasons (like autumn) 
that it has not seen before. 





This project was developed with the following Python Environment: 

**Python Version:**

Python 3.6.8 :: Anaconda custom (64-bit) 

**Installed Packages:** 

* absl-py==0.7.1
* alabaster==0.7.12
* anaconda-client==1.7.2
* anaconda-navigator==1.9.6
* anaconda-project==0.8.2
* appdirs==1.4.3
* appnope==0.1.0
* appscript==1.0.1
* asn1crypto==0.24.0
* astor==0.7.1
* astroid==2.2.5
* astropy==3.1.2
* atomicwrites==1.3.0
* attrs==19.1.0
* Automat==0.7.0
* Babel==2.6.0
* backcall==0.1.0
* backports.os==0.1.1
* backports.shutil-get-terminal-size==1.0.0
* beautifulsoup4==4.7.1
* bitarray==0.8.3
* bkcharts==0.2
* blaze==0.11.3
* bleach==3.1.0
* bokeh==1.0.4
* boto==2.49.0
* Bottleneck==1.2.1
* certifi==2019.3.9
* cffi==1.12.2
* cftime==1.0.3
* chardet==3.0.4
* Click==7.0
* cloudpickle==0.8.0
* clyent==1.2.2
* colorama==0.4.1
* conda==4.6.11
* constantly==15.1.0
* contextlib2==0.5.5
* cryptography==2.6.1
* cycler==0.10.0
* Cython==0.29.6
* cytoolz==0.9.0.1
* dask==1.1.4
* datashape==0.5.4
* decorator==4.4.0
* defusedxml==0.5.0
* distributed==1.26.0
* docutils==0.14
* entrypoints==0.3
* et-xmlfile==1.0.1
* fastcache==1.0.2
* filelock==3.0.10
* Flask==1.0.2
* Flask-Cors==3.0.7
* gast==0.2.2
* gevent==1.4.0
* glob2==0.6
* gmpy2==2.0.8
* greenlet==0.4.15
* grpcio==1.16.1
* gspread==0.6.2
* h5py==2.9.0
* heapdict==1.0.0
* html5lib==1.0.1
* httplib2==0.10.3
* hyperlink==18.0.0
* idna==2.8
* imageio==2.5.0
* imagesize==1.1.0
* importlib-metadata==0.0.0
* incremental==17.5.0
* ipykernel==5.1.0
* ipython==7.4.0
* ipython-genutils==0.2.0
* ipywidgets==7.4.2
* isort==4.3.16
* itsdangerous==1.1.0
* jdcal==1.4
* jedi==0.13.3
* Jinja2==2.10
* jsonschema==3.0.1
* jupyter==1.0.0
* jupyter-client==5.2.4
* jupyter-console==6.0.0
* jupyter-core==4.4.0
* jupyterlab==0.35.4
* jupyterlab-launcher==0.13.1
* jupyterlab-server==0.2.0
* Keras==2.2.4
* Keras-Applications==1.0.7
* Keras-Preprocessing==1.0.9
* keyring==18.0.0
* kiwisolver==1.0.1
* lazy-object-proxy==1.3.1
* llvmlite==0.28.0
* locket==0.2.0
* lxml==4.3.2
* Markdown==2.6.11
* MarkupSafe==1.1.1
* matplotlib==3.0.3
* mccabe==0.6.1
* mistune==0.8.4
* mkl-fft==1.0.10
* mkl-random==1.0.2
* more-itertools==6.0.0
* mpmath==1.1.0
* msgpack==0.6.1
* multipledispatch==0.6.0
* navigator-updater==0.2.1
* nbconvert==5.4.1
* nbformat==4.4.0
* netCDF4==1.4.2
* networkx==2.2
* nltk==3.4
* nose==1.3.7
* notebook==5.7.8
* numba==0.43.1
* numexpr==2.6.9
* numpy==1.16.2
* numpydoc==0.8.0
* oauth2client==4.1.2
* odo==0.5.1
* olefile==0.46
* openpyxl==2.6.1
* packaging==19.0
* pandas==0.24.2
* pandocfilters==1.4.2
* parso==0.3.4
* partd==0.3.10
* path.py==11.5.0
* pathlib2==2.3.3
* patsy==0.5.1
* pep8==1.7.1
* pexpect==4.6.0
* pickleshare==0.7.5
* Pillow==5.4.1
* pkginfo==1.5.0.1
* pluggy==0.9.0
* ply==3.11
* prometheus-client==0.6.0
* prompt-toolkit==2.0.9
* protobuf==3.7.0
* psutil==5.6.1
* ptyprocess==0.6.0
* py==1.8.0
* pyasn1==0.4.5
* pyasn1-modules==0.2.4
* pycodestyle==2.5.0
* pycosat==0.6.3
* pycparser==2.19
* pycrypto==2.6.1
* pycurl==7.43.0.2
* pyflakes==2.1.1
* Pygments==2.3.1
* pylint==2.3.1
* pyodbc==4.0.26
* pyOpenSSL==19.0.0
* pyparsing==2.3.1
* pyrsistent==0.14.11
* PySocks==1.6.8
* pytest==4.3.1
* pytest-arraydiff==0.3
* pytest-astropy==0.5.0
* pytest-doctestplus==0.3.0
* pytest-openfiles==0.3.2
* pytest-remotedata==0.3.1
* python-dateutil==2.8.0
* pytz==2018.9
* PyWavelets==1.0.2
* PyYAML==5.1
* pyzmq==18.0.0
* QtAwesome==0.5.7
* qtconsole==4.4.3
* QtPy==1.7.0
* requests==2.21.0
* rope==0.12.0
* rsa==3.4.2
* ruamel-yaml==0.15.46
* scikit-image==0.14.2
* scikit-learn==0.20.3
* scipy==1.2.1
* seaborn==0.9.0
* Send2Trash==1.5.0
* service-identity==18.1.0
* simplegeneric==0.8.1
* singledispatch==3.4.0.3
* six==1.12.0
* snowballstemmer==1.2.1
* sortedcollections==1.1.2
* sortedcontainers==2.1.0
* soupsieve==1.8
* Sphinx==1.8.5
* sphinxcontrib-websupport==1.1.0
* spyder==3.3.3
* spyder-kernels==0.4.2
* SQLAlchemy==1.3.1
* statsmodels==0.9.0
* sympy==1.3
* tables==3.5.1
* tblib==1.3.2
* tensorboard==1.10.0
* tensorflow==1.10.0
* termcolor==1.1.0
* terminado==0.8.1
* testpath==0.4.2
* Theano==1.0.4
* toolz==0.9.0
* torch==1.0.1.post2
* torchvision==0.2.2
* tornado==6.0.2
* tqdm==4.31.1
* traitlets==4.3.2
* Twisted==17.5.0
* typed-ast==1.3.1
* unicodecsv==0.14.1
* urllib3==1.24.1
* wcwidth==0.1.7
* webencodings==0.5.1
* Werkzeug==0.14.1
* widgetsnbextension==3.4.2
* wrapt==1.11.1
* wurlitzer==1.0.2
* xlrd==1.2.0
* XlsxWriter==1.1.5
* xlwings==0.15.4
* xlwt==1.2.0
* zict==0.1.4
* zipp==0.3.3
* zope.interface==4.6.0

